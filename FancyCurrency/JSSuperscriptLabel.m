//
//  JSSuperscriptLabel.m
//  FancyCurrency
//
//  Created by James Stewart on 3/11/13.
//  Copyright (c) 2013 Mutual Mobile. All rights reserved.
//

#import "JSSuperscriptLabel.h"
#import <CoreText/CoreText.h>

@implementation JSSuperscriptLabel

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self configureView];
    }
    return self;
}

- (void)awakeFromNib {
    [self configureView];
}

- (void)configureView {
    CGAffineTransform transform = CGAffineTransformMakeScale(1.0, -1.0);
    CGAffineTransformTranslate(transform, 0, -self.bounds.size.height);
    self.transform = transform;
    self.backgroundColor = [UIColor lightGrayColor];
}

- (void)drawTextInRect:(CGRect)rect {
    //[super drawRect:rect];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetTextMatrix(context, CGAffineTransformIdentity);
    CGFloat textRectVerticalOffset = [self textRectVerticalOffset];
    CGPoint textPosition = CGContextGetTextPosition(context);
    CGContextSetTextPosition(context, textPosition.x, textPosition.y + textRectVerticalOffset);
    CTLineRef line = CTLineCreateWithAttributedString((__bridge CFTypeRef) self.attributedText);
    CFArrayRef runs = CTLineGetGlyphRuns(line);
    
    CFIndex numOfRuns = CFArrayGetCount(runs);
    for (int index = 0; index < numOfRuns; ++index) {
        [self drawRunFromRuns:runs atIndex:index context:context];
    }
    
    CFRelease(line);
}

- (CGFloat)textRectVerticalOffset {
    // If label Rect is larger then textRect based on font size, then offset
    return 7.0f;
}

- (void)drawRunFromRuns:(CFArrayRef)runs atIndex:(NSInteger)index context:(CGContextRef)context {
    CTRunRef run = CFArrayGetValueAtIndex(runs, index);
    if ([self isSuperscriptRun:run]) {
        [self drawSuperscriptRunFromRuns:runs atIndex:index context:context];
    } else {
        CTRunDraw(CFArrayGetValueAtIndex(runs, index), context, CFRangeMake(0, 0));
    }
}

- (BOOL)isSuperscriptRun:(CTRunRef)run {
    CFDictionaryRef runAttributesRef = CTRunGetAttributes(run);
    NSDictionary *runAttributes = (__bridge NSDictionary*)runAttributesRef;
    return [[runAttributes allKeys] containsObject:@"JSSuperscriptName"];
}

- (void)drawSuperscriptRunFromRuns:(CFArrayRef)runs atIndex:(NSInteger)index context:(CGContextRef)context {
    CGFloat vertOffsetForSuperscript = [self verticalOffsetForSuperscriptFromRuns:(CFArrayRef)runs atIndex:index];
    CGPoint textPosition = CGContextGetTextPosition(context);
    CGContextSetTextPosition(context, textPosition.x, textPosition.y + vertOffsetForSuperscript);
    CTRunDraw(CFArrayGetValueAtIndex(runs, index), context, CFRangeMake(0, 0));
    // Reset text baseline
    CGPoint newTextPosition = CGContextGetTextPosition(context);
    CGContextSetTextPosition(context, newTextPosition.x, textPosition.y);
}

// Assumes that there are NOT 2 superscript runs adjacent
- (CGFloat)verticalOffsetForSuperscriptFromRuns:(CFArrayRef)runs atIndex:(NSInteger)index {
    NSInteger indexOffset = (index == 0) ? 1 : -1;
    CTRunRef superscriptRun = CFArrayGetValueAtIndex(runs, index);
    CTRunRef baselineRun = CFArrayGetValueAtIndex(runs, index + indexOffset);
    CGFloat tallAscent, shortAscent, descent, leading;
    CTRunGetTypographicBounds(baselineRun, CFRangeMake(0, 0), &tallAscent, &descent, &leading);
    NSLog(@"baseline font metrics, ascent: %f, descent: %f, leading: %f", tallAscent, descent, leading);
    CTRunGetTypographicBounds(superscriptRun, CFRangeMake(0, 0), &shortAscent, &descent, &leading);
    NSLog(@"superscript font metrics, ascent: %f, descent: %f, leading: %f", shortAscent, descent, leading);
    
    if ([self isMatchingAscenderSuperscriptRun:superscriptRun]) {
        return (tallAscent - shortAscent) - 1;
    } else {
        return 0.0;
    }
}

- (BOOL)isMatchingAscenderSuperscriptRun:(CTRunRef)run {
    NSDictionary *runAttributes = (__bridge NSDictionary*)CTRunGetAttributes(run);
    NSString *ascentName = [runAttributes valueForKey:@"JSSuperscriptName"];
    return [@"JSSuperscriptNameMatchAscender" isEqualToString:ascentName];
}

@end

