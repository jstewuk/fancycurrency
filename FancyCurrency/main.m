//
//  main.m
//  FancyCurrency
//
//  Created by James Stewart on 3/11/13.
//  Copyright (c) 2013 Mutual Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JSAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([JSAppDelegate class]));
    }
}
