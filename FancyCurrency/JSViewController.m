//
//  JSViewController.m
//  FancyCurrency
//
//  Created by James Stewart on 3/11/13.
//  Copyright (c) 2013 Mutual Mobile. All rights reserved.
//

#import "JSViewController.h"
#import "JSSuperscriptLabel.h"

@interface JSViewController ()
@property (weak, nonatomic) IBOutlet UILabel *currencyLabel;
@property (weak, nonatomic) IBOutlet JSSuperscriptLabel *superscriptLabel;

@end

@implementation JSViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self setupTextForLabel];
}

- (void) setupTextForLabel {
    NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
    
    attributes[NSFontAttributeName] = [UIFont systemFontOfSize:30];
    attributes[NSForegroundColorAttributeName] = [UIColor blueColor];
    
    NSMutableAttributedString *basicAttributedString = [[NSMutableAttributedString alloc] initWithString:@"12.34 and so on" attributes:attributes];
    
    
    UIFont *smallFont = [UIFont systemFontOfSize:16];
    UIFont *smallerFont = [UIFont systemFontOfSize:12];
    
    NSRange superscriptRange1 = NSMakeRange(2, 3);
    NSRange superscriptRange2 = NSMakeRange(5, 10);
    
    [basicAttributedString addAttribute:NSFontAttributeName value:smallFont range:superscriptRange1];
    [basicAttributedString addAttribute:@"JSSuperscriptName" value:@"JSSuperscriptNameMatchAscender" range:superscriptRange1 ];
    
    //[basicAttributedString addAttribute:NSFontAttributeName value:smallerFont range:superscriptRange2];
    //[basicAttributedString addAttribute:@"JSSuperscriptName" value:@"JSSuperscriptNameMatchAscender" range:superscriptRange2];
    
    self.currencyLabel.attributedText = basicAttributedString;
    
    self.superscriptLabel.attributedText = basicAttributedString;
}

@end
